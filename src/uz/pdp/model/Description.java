package uz.pdp.model;

import java.util.ArrayList;

public class Description {
    private String wordType;
    private ArrayList<String> examples;
    private ArrayList<String> synonymes;
    private String translation;

    public Description() {
    }

    public Description(String wordType, ArrayList<String> examples, ArrayList<String> synonymes, String translation) {
        this.wordType = wordType;
        this.examples = examples;
        this.synonymes = synonymes;
        this.translation = translation;
    }

    public String getWordType() {
        return wordType;
    }

    public void setWordType(String wordType) {
        this.wordType = wordType;
    }

    public ArrayList<String> getExamples() {
        return examples;
    }

    public void setExamples(ArrayList<String> examples) {
        this.examples = examples;
    }

    public ArrayList<String> getSynonymes() {
        return synonymes;
    }

    public void setSynonymes(ArrayList<String> synonymes) {
        this.synonymes = synonymes;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    @Override
    public String toString() {
        return "Description{" +
                "wordType='" + wordType + '\'' +
                ", examples=" + examples +
                ", synonymes=" + synonymes +
                ", translation='" + translation + '\'' +
                '}';
    }
}
