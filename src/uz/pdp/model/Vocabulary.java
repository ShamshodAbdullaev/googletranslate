package uz.pdp.model;

import java.util.*;

public class Vocabulary {
    Scanner scanner = new Scanner(System.in);

    HashMap<String, Description> words = new HashMap<>();

    public void addWord() {
        scanner = new Scanner(System.in);
        System.out.print("So'zni kiriting: ");
        String key = scanner.nextLine();

        Description value = new Description();
        System.out.print("So'z turkumi: ");
        value.setWordType(scanner.nextLine());
        value.setExamples(getList("Misol"));
        value.setSynonymes(getList("Synonym"));
        System.out.print("Tarjimasi: ");
        value.setTranslation(scanner.nextLine());
        words.put(key, value);
    }

    public void deleteWord() {
        scanner = new Scanner(System.in);
        System.out.print("So'zni kiriting: ");
        String key = scanner.nextLine();
        if (words.containsKey(key)) {
            words.remove(key);
        } else {
            System.out.print("So'z topimadi.");
        }
    }

    public void find() {
        scanner = new Scanner(System.in);
        System.out.print("So'zni kiriting: ");

        ArrayList<String> foundWords = findWords(scanner.nextLine());
        System.out.println("So'zni tanlang: ");
        for (String foundWord : foundWords) {
            System.out.println((foundWords.indexOf(foundWord) + 1) + "=>" + foundWord);
        }
        scanner = new Scanner(System.in);
        System.out.print("kerak so'zni tanlang(indexda): ");
        int index = scanner.nextInt();
        String key = foundWords.get(index - 1);
        System.out.println(words.get(key));

    }

    public void search() {
        scanner = new Scanner(System.in);
        System.out.print("So'zni kiriting: ");
        Map<String, Description> searchWords = searchWords(scanner.nextLine());
        searchWords.forEach((s, description) -> System.out.println(s+"=>"+description));
    }


    private ArrayList<String> getList(String attribute) {
        scanner = new Scanner(System.in);
        ArrayList<String> attributes = new ArrayList<>();
        String example = "step";
        int index = 0;
        while (!example.equals("")) {
            index++;
            System.out.print(index + "-" + attribute + "ni kiriting: ");
            example = scanner.nextLine();
            if (!example.equals("")) {
                attributes.add(example);
            }
        }
        return attributes;
    }

    private ArrayList<String> findWords(String word) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> foundWords = new ArrayList<>();
        Set<String> keys = words.keySet();
        keys.forEach(s -> {
            if (s.startsWith(word)) {
                foundWords.add(s);
            }
        });
        Collections.sort(foundWords);
        return foundWords;
    }

    private Map<String, Description> searchWords(String word) {
        Scanner scanner = new Scanner(System.in);
        Map<String, Description> result = new HashMap<>();
        Set<Map.Entry<String, Description>> entries = words.entrySet();
        for (Map.Entry<String, Description> entry : entries) {
            String key = entry.getKey();
            Description value = entry.getValue();
            if (key.contains(word)) {
                result.put(key, value);
            } else if (value.getWordType().contains(word)) {
                result.put(key, value);
            } else if (value.getTranslation().contains(word)) {
                result.put(key,value);
            }else {
                for (String example : value.getExamples()) {
                    if (example.contains(word)){
                        result.put(key,value);
                        break;
                    }
                }
                for (String synonym : value.getSynonymes()) {
                    if (synonym.contains(word)){
                        result.put(key,value);
                        break;
                    }
                }
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return "Vocabulary{" +
                "scanner=" + scanner +
                ", words=" + words +
                '}';
    }

}
