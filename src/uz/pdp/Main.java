package uz.pdp;

import uz.pdp.model.Vocabulary;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Vocabulary vocabulary = new Vocabulary();

        int stepCode = 1;
        while (stepCode != 0) {
            scanner = new Scanner(System.in);
            System.out.println("0=> Back, 1=> addWords, 2=> findWords, 3=> searchWords, 4=>deleteWords, 5=> viewWords");
            System.out.print("Menu: ");
            stepCode = scanner.nextInt();
            switch (stepCode) {
                case 0:
                    break;
                case 1:
                    vocabulary.addWord();
                    break;
                case 2:
                    vocabulary.find();
                    break;
                case 3:
                    vocabulary.search();
                    break;
                case 4:
                    vocabulary.deleteWord();
                    break;
                case 5:
                    System.out.println(vocabulary.toString());
                    break;
                default:
                    System.out.println("Menudagi sonlarni kiriting.");
            }

        }

    }
}
